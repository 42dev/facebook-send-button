=== Facebook Send Button Plugin ===

Contributors: beerf
Tags: facebook, send, button
Requires at least: 3.0
Tested up to: 3.1
Donate Link: -
Stable Tag: trunk

Displays the Fourrsquare Send Button on each post.

== Description ==

Adds a Facebook Send Button on each post with the correct permalink.

== Installation ==

1. Upload `facebook-send-button.php` to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

= Where will the button show up? =

Currently it will show under the content of each post in WordPress.
Future versions will make the position configurable.

== Screenshots ==

== Changelog ==

= 0.1 =
* First public release.

== Upgrade Notice ==

Upgrade through WordPress backend.